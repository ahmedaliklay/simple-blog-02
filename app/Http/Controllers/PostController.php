<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Validator;
class PostController extends Controller
{
    public function createPost(Request $request){

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required',
        ]);
        if($validator->fails()){
            return response()->json(['status'=>'error','errors'=>$validator->errors()]);
        }
        $data = new Post;
        $data->title = $request->title;
        $data->body = $request->body;
        $data->save();
        return response()->json(['status'=>'success','data'=>$data]);
    }
    public function showPost(){
        $posts = Post::all();
        return response()->json(['status'=>'success','data'=>$posts]);
    }
    public function updateData(Request $request,Post $id){
        $data = $id->update($request->all());
        return response()->json(['status'=>'success','data'=>$data]);
    }
}
