//////////

import Register from './components/Register'

import Login from './components/Login'

import Post from './components/Post'

////////////
const routes = [
  { path: '/login', component: Login },
  { path: '/register', component: Register },
  { path: '/post', component: Post },
]
export default routes