

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'

//sweetalert2
import Swal from 'sweetalert2'
window.Swal = Swal
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})
window.Toast = Toast


Vue.use(VueRouter)

//MainHeader
import MainHeader from './components/Main-Header'

//routes
import routes from './routers'

const router = new VueRouter({
    mode:'history',
    routes // short for `routes: routes`
})
const app = new Vue({
    el: '#app',
    router,
    components:{
        MainHeader
    }
});
